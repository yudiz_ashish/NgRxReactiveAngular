import {BookEffects} from './books.effects';

export const effects:any[] = [BookEffects];

export * from './books.effects';